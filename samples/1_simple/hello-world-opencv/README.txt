To start working with this project you must have cmake installed on your computer. 
Using cmake a Makefile will be generated, file which can be imported in an IDE such Eclipse.

To generate Makefile and to build it:
sh: cmake .
sh: make

To generate Makefile, .project and .cproject Debug version for Eclipse CDT:
sh: cd ..
sh: mkdir project-name_Debug
sh: cd project-name_Debug
sh: cmake -G"Eclipse CDT4 - Unix Makefiles" -D CMAKE_BUILD_TYPE=Debug ../project-name 

Details on how to import a CMake project in Eclipse:
https://cmake.org/Wiki/CMake:Eclipse_UNIX_Tutorial
https://cmake.org/Wiki/Eclipse_CDT4_Generator
